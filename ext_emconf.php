<?php
$EM_CONF['form_legacy'] = [
    'title' => 'Form Legacy',
    'description' => 'Legacy version 7 Form Library, Plugin and Wizard',
    'category' => 'plugin',
    'state' => 'beta',
    'clearCacheOnLoad' => true,
    'author' => 'Patrick Broens, Ralf Zimmermann',
    'author_email' => 'patrick@patrickbroens.nl, ralf.zimmermann@tritum.de',
    'author_company' => '',
    'version' => '11.0.12',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-8.7.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
